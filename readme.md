# Many Amigos - Web Developer Test

----

## Installation

### 1. Clone it

To install, first clone a copy of the project into your file system:

`git clone git@bitbucket.org:k7n4n5t3w4rt/web-developer-test.git`

### 2. Get Dependencies

- Go to the document root
- Run `composer install`
- Run `npm install`

### 3. Edit the Hosts File (Dev environments only)

Edit the `/etc/hosts` file with your favourite editor and add `127.0.0.1 manyamigos.dev`

### 4. Edit the Apache 2 Virtual Host (Dev environments only)

The following step assumes that `httpd-vhosts.conf` is being included in `httpd.conf`:

 - Default path in Mac OSX is `/private/etc/apache2/extra/httpd-vhosts.conf`

Add the following:

    <VirtualHost *:80>
        DocumentRoot "/path/to/web-developer-test/public"
        ServerName manyamigos.dev
        <Directory /path/to/web-developer-test/public>
            AllowOverride All
            Order allow,deny
            Allow from all
        </Directory>
    </VirtualHost>

### 5. Restart Apache

Restart apache by running `sudo apachectl restart`

----

## Front End

### 1. Assets

Assets go on the `public/assets` directory, and are compiled by gulp.

### 2. Building

To build front end into `public` directory, use `gulp`. You might need to install gulp first, but that's easy: `npm install -g gulp`


----

## Testing

To setup Codeception on server:

    wget http://chromedriver.storage.googleapis.com/2.10/chromedriver_linux64.zip
    unzip chromedriver_linux64.zip
    sudo mv chromedriver /usr/bin/chromedriver

To test, run `vendor/bin/codecept run --steps`
