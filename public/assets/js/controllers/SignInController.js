angular.module('manyAmigosClient').controller('SignInController', ['$scope', '$window', 'UserManager',
    function($scope, $window, UserManager) {

    // Initialise credential
    $scope.credential = {};
    $scope.startOpen  = false;
    $scope.redirectTo = null;

    // Due to the nature of the front end, we are getting Laravel rendered
    // views, hence we revert to vanilla javascript to get the query params
    // since Angular's $location.search() seems to only work after the #
    // view insertion or whilst using html5Mode, both of which are not
    // necessary in this implementation
    var getQueryString = function() {
        var result      = {},
            queryString = $window.location.search.slice(1),
            pattern     = /([^&=]+)=([^&]*)/g,
            m;
        while (m = pattern.exec(queryString)) {
            result[decodeURIComponent(m[1])] = decodeURIComponent(m[2]);
        }

        return result;
    };

    // Store information from the query string if available
    angular.forEach(getQueryString(), function(value, key) {
        switch (key) {
            case 'doLogin':
                $scope.startOpen = (value.toString() === '1');
                break;
            case 'next':
                $scope.redirectTo = value;
                break;
        }
    });

    // Submit credential
    $scope.signIn = function(e) {
        // Not sure why this is necessary, but it is...
        e.preventDefault();

        var credential = angular.copy($scope.credential);
        UserManager.authenticate(credential.username, credential.password).then(
            // On success
            function() {

            },
            // On failure
            function() {

            }
        )

        return false;
    };

    // If start open is true, force the modal to open
    // This isn't ideal for angular, but for the public facing site
    // we just use angular jQuery style for the most part anyway...
    if ($scope.startOpen) {
        _.defer(function() {
            angular.element('#sign-in-modal').foundation('reveal', 'open');
        });
    }

}]);