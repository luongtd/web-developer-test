registerTest ('Product complaint test', {
    setup: function($) {
        $('#first_name').val('John');
        $('#surname').val('Smith');
        $('#email').val('email@example.com');
        $('#contact_number').val('1234');
        $('#address').val('123 Main Street');
        $('#suburb').val('New York');
        $('#state').val('NSW');
        $('#postcode').val('11111');
        $('#enquiry_type').val('Product complaint');
        $('[type="submit"]').click();
    },
    load: function() {
        this.test ("The form cannot be submitted without product info when Enquiry Type is 'Product complaint'", function($){
            // Check the error messages container 
            ok(!$('div#errors').length, 'Passed');
        });
    }
});