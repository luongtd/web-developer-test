registerTest ('Require fields test', {
    setup: function($) {
        $('input[type="submit"]').click();
    },
    load:function() {
        this.test("The form cannot be submitted without required fields?", function($) {
            // Check the error messages container 
            ok(!$('div#errors').length, 'Passed');
        });        
    }
});