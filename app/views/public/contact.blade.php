@extends('layouts.public')

@section('content')

<div class="row">
    <div class="medium-11 columns small-offset-1">
        <?php if($errors->has()) :?>
            <div id="errors">
                <ul>
                    <?php foreach($errors->getMessages() as $key => $msg) :?>
                        <li><?php echo $msg[0] ?></li>
                    <?php endforeach;?>
                </ul>
            </div>
        <?php endif?>
        {{ Form::open(array('method' => 'POST', 'id' => 'form-contact')) }}
            <div class="row">
                <div class="small-8">
                    <!-- Contact info -->
                    <div class="row">
                        <div class="small-4 columns">
                            {{ Form::label('first_name', null, array('class' => 'right inline')) }}
                        </div>
                        <div class="small-8 columns">
                            {{ Form::text('first_name', Input::old('first_name'), array('required')) }}
                        </div>
                    </div>
                    <div class="row">
                        <div class="small-4 columns">
                            {{ Form::label('surname', null, array('class' => 'right inline')) }}
                        </div>
                        <div class="small-8 columns">
                            {{ Form::text('surname', Input::old('surname'), array('required')) }}
                        </div>
                    </div>            
                    <div class="row">
                        <div class="small-4 columns">
                            {{ Form::label('email', 'Email address', array('class' => 'right inline')) }}
                        </div>
                        <div class="small-8 columns">
                            {{ Form::email('email', Input::old('email'), array('required')) }}
                        </div>
                    </div>      
                    <div class="row">
                        <div class="small-4 columns">
                            {{ Form::label('contact_number', 'Daytime Contact Number', array('class' => 'right inline')) }}
                        </div>
                        <div class="small-8 columns">
                            {{ Form::text('contact_number', Input::old('contact_number'), array('required')) }}
                        </div>
                    </div>     
                    <br>
                    
                    <!-- Address info -->
                    <div class="row">
                        <div class="small-4 columns">
                            {{ Form::label('address', null, array('class' => 'right inline')) }}
                        </div>
                        <div class="small-8 columns">
                            {{ Form::text('address', Input::old('address'), array('required')) }}
                        </div>
                    </div>     
                    <div class="row">
                        <div class="small-4 columns">
                            {{ Form::label('suburb', null, array('class' => 'right inline')) }}
                        </div>
                        <div class="small-8 columns">
                            {{ Form::text('suburb', Input::old('suburb'), array('required')) }}
                        </div>
                    </div>      
                    <div class="row">
                        <div class="small-4 columns">
                            {{ Form::label('state', null, array('class' => 'right inline'), array('required')) }}
                        </div>
                        <div class="small-8 columns">
                            {{ 
                                Form::select(
                                    'state', 
                                    array(
                                      '' => "-- Select --", 
                                        'ACT' => 'ACT', 
                                        'NSW' => 'NSW', 
                                        'NT' => 'NT', 
                                        'QLD' => 'QLD', 
                                        'SA' => 'SA', 
                                        'TAS' => 'TAS', 
                                        'VIC' => 'VIC', 
                                        'WA' => 'WA'
                                      ), 
                                    Input::old('state'), 
                                    array('required')
                                )
                            }}
                        </div>
                    </div>     
                    <div class="row">
                        <div class="small-4 columns">
                            {{ Form::label('postcode', null, array('class' => 'right inline')) }}
                        </div>
                        <div class="small-8 columns">
                            {{ Form::text('postcode', Input::old('postcode'), array('required')) }}
                        </div>
                    </div> 
                    <br>
                    <!-- Enquiry info -->
                    <div class="row">
                        <div class="small-4 columns">
                            {{ Form::label('enquiry_type', null, array('class' => 'right inline')) }}
                        </div>
                        <div class="small-8 columns">
                            {{ 
                                Form::select(
                                    'enquiry_type', 
                                    array(
                                        '' => "-- Select --", 
                                        'General enquiry' => "General enquiry", 
                                        'Product feedback or enquiry' => 'Product feedback or enquiry', 
                                        'Product complaint' => 'Product complaint'
                                    ), 
                                    Input::old('enquiry_type'), 
                                    array('required') 
                                ) 
                            }}
                        </div>
                    </div>      
  
                    <!-- Product info -->
                    <div class="row">
                        <div class="small-4 columns">
                            {{ Form::label('product_name', null, array('class' => 'right inline')) }}
                        </div>
                        <div class="small-8 columns">
                            {{ Form::text('product_name', Input::old('product_name')) }}
                        </div>
                    </div>         
                    <div class="row">
                        <div class="small-4 columns">
                            {{ Form::label('product_size', null, array('class' => 'right inline')) }}
                        </div>
                        <div class="small-8 columns">
                            {{ Form::text('product_size', Input::old('product_size')) }}
                        </div>
                    </div>     
                    <div class="row">
                        <div class="small-4 columns">
                            {{ Form::label('use_by_date', 'Use-by date', array('class' => 'right inline')) }}
                        </div>
                        <div class="small-8 columns">
                            {{ Form::text('use_by_date', Input::old('use_by_date')) }}
                        </div>
                    </div>  
                    <div class="row">
                        <div class="small-4 columns">
                            {{ Form::label('batch_code', null, array('class' => 'right inline')) }}
                        </div>
                        <div class="small-8 columns">
                            {{ Form::text('batch_code', Input::old('batch_code')) }}
                        </div>
                    </div>  
                    <div class="row">
                        <div class="small-4 columns">
                            {{ Form::label('enquiry', null, array('class' => 'right inline')) }}
                        </div>
                        <div class="small-8 columns">
                            {{ Form::textarea('enquiry', Input::old('enquiry'), array('rows' => 5)) }}
                        </div>
                    </div>                    
                    <div class="row">
                        <button type="submit" class="right">Submit</button>
                    </div>
                </div>
            </div>
        {{ Form::close() }} 
    </div>
</div>

@stop