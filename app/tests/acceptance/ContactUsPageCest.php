<?php
use \WebGuy;

class ContactUsPageCest
{
    /**
     * Checks that the content is present on contact us page
     *
     * @param WebGuy $I
     */
    public function checkContent(WebGuy $I)
    {
        $I->wantTo('check that the content is present in page');
        $I->amOnpage('/contact-us');
        $I->see('Contact Us');
    }

    /**
     * Checks that the main menu element is in the DOM
     *
     * @param WebGuy $I
     */
    public function checkMainMenuIsInDOM(WebGuy $I)
    {
        $I->wantTo('ensure menu is present in DOM');
        $I->amOnpage('/contact-us');
        $I->seeElementInDOM('#main-menu');
    }

    /**
     * Check for Google Analytics on page
     *
     * @param WebGuy $I
     */
    public function checkForGoogleAnalyticsInDOM(WebGuy $I)
    {
        $I->wantTo('check that Google analytics is present on page');
        $I->amOnPage('/contact-us');
        $I->seeInPageSource('UA-52164821-1');
    }

}