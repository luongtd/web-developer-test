<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

// Public facing Pages
Route::get('/', array('as' => 'home', 'uses' => 'PublicController@showHomePage'));
Route::get('/how-it-works', 'PublicController@showHowItWorksPage');
Route::get('/how-it-works/raising-money', 'PublicController@showRaisingMoneyPage');
Route::get('/how-it-works/investing', 'PublicController@showInvestingPage');
Route::get('/privacy-policy', 'PublicController@showPrivacyPolicyPage');
Route::get('/faq', 'PublicController@showFaqPage');
Route::get('/terms-of-use', 'PublicController@showTermsOfUsePage');
Route::get('/contact-us', 'PublicController@showContactPage');
Route::post('/contact-us', 'PublicController@postEnquiry');
Route::get('/members', 'MemberController@showMembersList');
Route::get('/member/{code}', 'MemberController@showMember');
Route::get('/sign-up', 'UserController@showSignUpPage');
// For the time being, just use modal partial for logging in
// Route::get('/sign-in', 'UserController@showSignInPage');

// Sign in Page for administrator
Route::get('/ma-sign-in', 'UserController@showAdministratorSignInPage');
Route::get('/ma-sign-out', 'UserController@signOutAdministrator');

// User actions
Route::post('/sign-in', 'UserController@signInUser');
Route::post('/sign-out', 'UserController@signOutUser');
Route::post('/sign-up', 'UserController@signUpUser');
Route::post('/ma-sign-in', 'UserController@signInAdministrator');

// Interface Page
Route::get('/interface', 'InterfaceController@showInterfacePage');

